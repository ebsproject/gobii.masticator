package org.gobii.masticator.aspects;

public class RowAspect extends CoordinateAspect {
	public RowAspect(Integer row, Integer col) {
		super(row, col);
	}
}
